# Podstawowa konfiguracja Goxy

Goxy jest pełnoprawnym zamiennikiem dotychczasowego proxy na twoim serwerze. Działa w modelu **SaaS** - Software as a Service - co oznacza, że nie musisz martwić się o instalowanie czegokolwiek na własnej maszynie, a konfiguracja przebiega błyskawicznie w [naszym panelu](https://panel.goxy.pl).  
W tej części dowiesz się jak poprawnie połączyć swój serwer Minecraft z Goxy. Poradnik składa się z kilku etapów:

1. [Tworzenie serwera Goxy](#tworzenie-serwera-goxy)
2. [Generowanie klucza dostępu](#generowanie-klucza-dostepu)
3. [Konfiguracja serwera Minecraft](#konfiguracja-serwera-minecraft)
4. [Serwer pierwszego wyboru](#serwer-pierwszego-wyboru)

---

## Tworzenie serwera Goxy
Cała konfiguracja przebiega w [Panelu Klienta](https://panel.goxy.pl).

![Panel](https://i.imgur.com/2CJjxiu.png)


W celu utworzenia nowego serwera:
1. Udaj się do [Panelu Klienta](https://panel.goxy.pl).
2. Przejdź do zakładki **Sieci** z menu znajdującego się po lewej stronie.  
   ![](https://i.imgur.com/hO1hpT8.png)
3. Wybierz interesującą cie sieć.
4. Przejdź do zakładki **Serwery** z menu wybranej sieci.  
   ![](https://i.imgur.com/KYkbsZ0.png)
5. Utwórz nowy folder.  
   ![](https://i.imgur.com/41AGWI9.png)  
   Folder jest zwykłym kontenerem na serwery. W celu uniknięcia komplikacji, przy niektórych funkcjach Goxy lepiej jest tworzyć osobny folder dla każdego trybu na twoim serwerze.  
   Nazwa folderu może być dowolna, lecz dla wygody lepiej nadać taką, jak nazywać się będzie serwer.
6. Utwórz nowy serwer.  
   ![](https://i.imgur.com/Hha9CWD.png)  
   Wypełnij wszystkie dane poprawnie. Nie zapomnij przypisać serwera do wcześniej utworzonego folderu!  
   ![](https://i.imgur.com/hpaqvsO.png)
7. Sprawdź czy wykonałeś wszystkie kroki poprawnie, struktura serwerów powinna wyglądać tak:
   ![](https://i.imgur.com/EC4QnEP.png)

---

## Generowanie klucza dostępu
Klucz dostępu, można powiedzieć, że jest w pewnym sensie hasłem do serwera.  
Każdy serwer podłączony do Goxy musi mieć własny, wygenerowany klucz. Bez tego późniejsze wejście na serwer nie będzie możliwe.

W celu wygenerowania nowego klucza dostępu:
1. Przejdź do ustawień swojej sieci.  
   ![](https://i.imgur.com/nQXnbph.png)
2. W drugiej kolumnie znajdziesz klucze dostępu.  
   ![](https://i.imgur.com/kkRqSGf.png)
3. Utwórz nowy klucz dostępu.  
   ![](https://i.imgur.com/8sgUbHr.png)  
   Nadaj mu nazwę i przypisz go do odpowiedniego folderu oraz serwera.  
   ![](https://i.imgur.com/7ufwMK5.png)  
   Zapisz go w bezpiecznym miejscu, bo w przyszłości nie będzie możliwości zobaczenia go!
   ![](https://i.imgur.com/okVMVZG.png)
4. Sprawdź czy wykonałeś wszystkie kroki poprawnie, powinieneś widzieć na liście świeżo wygenerowany klucz dostępu:

   ![](https://i.imgur.com/8VGAmC7.png)

---

## Konfiguracja serwera Minecraft

Goxy wspiera serwery Minecraft oparte o silnik Spigot od wersji 1.8.9, aż do najnowszej.

W celu konfiguracji serwera Minecraft:
1. Wyłącz serwer, który będziesz konfigurował.
2. Pobierz oficjalny plugin Goxy z naszego [Discorda](https://discord.gg/7bTwzpDueM).
3. Przenieś pobrany plugin do folderu **plugins**, w głównym katalogu twojego serwera.
4. Uruchom serwer, aby wygenerowała się konfiguracja pluginu.
5. Wyłącz ponownie serwer, aby uniknąć nadpisania danych.
6. Otwórz plik **config.yml** z folderu **plugins/goxy**.  
   Zawartość pliku powinna wyglądać w następujący sposób:
   ```yaml
   api:
      token: <tutaj wklej klucz>
   ```
7. W polu **token** w sekcji **api** wpisz wygenerowany wcześniej klucz dostępu serwera.
8. Uruchom ponownie serwer Minecraft.

---

## Serwer pierwszego wyboru

Ostatnim krokiem jest wybraniem folderu, na który Goxy będzie kierować graczy po wejściu.  
Zrobisz to w **Liście serwerów** na samym końcu: 
![](https://i.imgur.com/X6oCIVm.png)

Rozwiń listę i wybierz interesujący cie folder.

---

## To koniec!

Konfiguracja Goxy jest banalnie prosta i zajmuje kilka minut.  
Jeżeli zrobiłeś wszystko poprawnie, powinieneś móc dołączyć na serwer korzystając z jednej ze zweryfikowanych domen twojej sieci.  
![](https://i.imgur.com/wnUwrLE.png)

---

## Co dalej?

Goxy jest rozbudowanym narzędziem dla administratorów serwerów Minecraft.  
Wiele autorskich rozwiązań oraz opcji dostosowania proxy do własnych potrzeb. To wszystko dla Ciebie.

Następne kroki:
1. [Dodawanie nowej domeny]()
2. [Podstawowe ustawienia Goxy]()
3. [Najczęściej powtarzające się problemy]()
4. [Najczęściej zadawane pytania]()


**Masz pytanie, którego nie obejmuje dokumentacja?**  
Dołącz na nasz oficjalny serwer [Discord](https://discord.gg/3d8vU2Dsrb)!
